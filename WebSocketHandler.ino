void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t length) {
    switch(type) {
        case WStype_DISCONNECTED:
            Serial.printf("[%u] Disconnected!\n", num);
            break;
            
        case WStype_CONNECTED: {
            IPAddress ip = webSocket.remoteIP(num);
            Serial.printf("[%u] Connected from %d.%d.%d.%d url: %s\n", num, ip[0], ip[1], ip[2], ip[3], payload);
            Serial.printf("[%u] configToJson: %s\n", num, configToJson(config, PASSWORD_HIDE).c_str());
            webSocket.sendTXT(num, "c" + configToJson(config, PASSWORD_HIDE));
            blink(3, 1023);
        }
            break;
            
        case WStype_TEXT:
            Serial.printf("[%u] received TEXT: %s\n", num, payload);
            handleWSMessages(num, payload);
            break;
    }
}

void handleWSMessages(uint8_t num, uint8_t * payload) {
    switch(payload[0]) {
        case 'l':{
            uint16_t clientLedValue = (uint16_t) strtol((const char *) &payload[1], NULL, 10);
            Serial.printf("[%u] clientLedValue: %d\n", num, clientLedValue);
            config.ledValue = clientLedValue;
            led(config.ledValue);
//            broadcastLedValueChange(num, clientLedValue);
        }
            break;
            
        case 'c':{
            String configStringified = String((char*) payload).substring(1);
            Serial.printf("[%u] config: %s\n", num, configStringified.c_str());
            Config clientConfig = jsonToConfig(configStringified);
            clientConfig = overridePasswordIfNeeded(clientConfig, config.password);
            eepromPut(configToJson(clientConfig));
            
            String configStringifiedEeprom = eepromGet();
            Serial.println("configStringifiedEeprom = " + configStringifiedEeprom);
            webSocket.sendTXT(num, 'i' + String("configStringifiedEeprom=" + String(configStringifiedEeprom)));
            config = jsonToConfig(configStringifiedEeprom);
            Serial.println("configToJson = " + configToJson(config));
        }
            break;
        
        //load config
        case 'r':{
            Serial.printf("[%u] data: Request. Send config to client: %s\n", num, configToJson(config, PASSWORD_HIDE).c_str());
            webSocket.sendTXT(num, "c" + configToJson(config, PASSWORD_HIDE));
        }
            break;
            
        //reset ESP8266
        case 'd':{
            Serial.printf("[%u] data: %s\n", num, "Request. Reset ESP8266.");
            //            eepromPut(""); // erase eeprom
            webSocket.sendTXT(num, "i" + String("Restaring esp8266."));
            delay(200);
            ESP.restart();
        }
            break;
    }
}

void broadcastLedValueChange(uint8_t num, uint16_t clientLedValue) {
    for (int i = 0; i < webSocket.connectedClients(false); i++) {
        if (i != num) {
//            webSocket.sendTXT(i, "{\"t\":\"l\",\"v\":\"" + String(clientLedValue) + "\"}");
            webSocket.sendTXT(num, "l" + String(config.ledValue));
        }
    }
}
