void eepromPut(String buffer) {
    char charBuffer[512];
    buffer.toCharArray(charBuffer, sizeof(charBuffer));
    EEPROM.begin(CONFIG_BUFFER_SIZE);
    delay(10);
    EEPROM.put(0, charBuffer);
//    EEPROM.put(0, charBuffer);
    EEPROM.commit();
    EEPROM.end();
    blink(1, 1023);
    delay(50);
    Serial.println("EEPROM written = " + String(eepromGet()) + "(" + sizeof(charBuffer) + ")");
}

String eepromGet() {
    EEPROM.begin(CONFIG_BUFFER_SIZE);
    char charBuffer[512];
    EEPROM.get(0, charBuffer);
    EEPROM.end();
    return String(charBuffer);
}

//Deprecated
void eepromWrite(byte value) {
    EEPROM.begin(1);
    EEPROM.write(0, value);
    EEPROM.end();
    blink(1, 1023);
    Serial.printf("EEPROM value = %d\n", EEPROM.read(0));
}

//Deprecated
byte eepromRead() {
    EEPROM.begin(1);
    byte value = EEPROM.read(0);
    Serial.printf("EEPROM value = %d\n", value);
    EEPROM.end();
    return value;
}
