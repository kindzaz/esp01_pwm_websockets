Config jsonToConfig(String configJson) {
    StaticJsonDocument<512> jsonDocument;
    auto err = deserializeJson(jsonDocument, configJson);
    if (err) {
        Serial.println("ERROR. Parsing config json failed: " + configJson + ". Clearing eeprom and creating default config.");
        return getDefaultConfig();
    }
    
    JsonObject root = jsonDocument.as<JsonObject>();
    Config config;
    config.ledValue = root["ledValue"];
    strlcpy(config.ssid, root["ssid"], sizeof(config.ssid));
    strlcpy(config.password, root["password"], sizeof(config.password));
    strlcpy(config.domainName, root["domainName"], sizeof(config.domainName));
    config.dimTime = root["dimTime"];
    return config;
}

Config getDefaultConfig() {
    Config config;
    config.ledValue = 0;
    strlcpy(config.ssid, "", sizeof(config.ssid));
    strlcpy(config.password, "", sizeof(config.password));
    strlcpy(config.domainName, "", sizeof(config.domainName));
    config.dimTime = 1000;
    return config;
}

Config overridePasswordIfNeeded(Config config, char overridePassword[]) {
    if (strcmp(config.password, PASSWORD_HIDDEN) == 0) {
        strlcpy(config.password, overridePassword, sizeof(config.password));
    }

    return config;
}

String configToJson(Config config) {
    return configToJson(config, false);
}

String configToJson(Config config, bool hidePassword) {
    String json;
    StaticJsonDocument<512> jsonDocument;
    JsonObject root = jsonDocument.to<JsonObject>();
    root["ledValue"] = config.ledValue;
    root["ssid"] = config.ssid;
    root["password"] = hidePassword ? PASSWORD_HIDDEN : config.password;
    root["domainName"] = config.domainName;
    root["dimTime"] = config.dimTime;
    serializeJson(root, json);
    return json;
}

void blink(short times, short pwm) {
    for (int i = 0; i < times; i++) {
        led(pwm);
        delay(30);
        led(0);
        delay(30);
    }
    
    led(config.ledValue);
}

void dimUp() {
    int startTime = millis();
    int deltaTime = 0;
    int ledValue = 0;
    float timeFactor = 0;
    while (deltaTime <= config.dimTime) {
        deltaTime = millis() - startTime;
        timeFactor = (float) deltaTime / config.dimTime;
        ledValue =  pow(timeFactor, 1.5) * config.ledValue; 
        led(ledValue);
//        Serial.printf("dimUp:deltaTime=%d; deltaTime / config.dimTime = %d ; ledValue=%d \r\n", deltaTime, deltaTime / config.dimTime, ledValue);
    }
}
