/*
 * WebSocketServer_LEDcontrol.ino
 *
 *  Created on: 26.11.2015
 *
 *git: 
 *  https://stackoverflow.com/questions/5601931/what-is-the-best-and-safest-way-to-merge-a-git-branch-into-master
 *
 *https://myelectronicslab.com/esp8266-pwm-example-led-dimmer/
 *http://esp8266.github.io/Arduino/versions/2.0.0/doc/libraries.html#mdns-and-dns-sd-responder-esp8266mdns-library
 *https://bitbucket.org/kindzaz/esp01_pwm_websockets/src/master/
 *https://arduinojson.org/v6/assistant/
 *libs
 *  C:\Users\kindz\OneDrive\Documents\ArduinoData\packages\esp8266\hardware\esp8266\2.4.2\libraries\EEPROM
 *  TODO:
 *      OK* settings: add "domainName" to struct config;
 *      OK* settings: send setting to user;
 *      OK* settings: server side, load on startup, save;
 *      OK* settings: show/hide html fields by clicking on button;
 *      OK* settings: add restart functionality;
 *      OK* settings: password visibility?;
 *      OK* settings: add field dimTime;
 *      OK* dimUp algorithm;
 *      * WebSocketHandler: change request handling;
 *      * ...
 */

#include <Arduino.h>
#include <EEPROM.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <WebSocketsServer.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <Hash.h>
#include <ArduinoJson.h>
#define PIN_LED 0
#define WIFI_TIMEOUT 10
#define WIFI_SOFT_AP_SSID "GLED"
#define PASSWORD_HIDE true
#define PASSWORD_HIDDEN "***"
#define CONFIG_BUFFER_SIZE 1024

ESP8266WiFiMulti wifiMulti;       // Create an instance of the ESP8266WiFiMulti class, called 'wifiMulti'
ESP8266WebServer server(80);       // Create a webserver object that listens for HTTP request on port 80
WebSocketsServer webSocket(81);    // create a websocket server on port 81
struct Config {
    char dataType[10];
    bool error;
    int ledValue;
    char ssid[32];
    char password[32];
    char domainName[32];
    int dimTime;
} config;

void setup() {
    Serial.begin(9600);
//    Serial.setDebugOutput(true);
//    pinMode(PIN_LED, OUTPUT);
    led(0);
    config = jsonToConfig(eepromGet());
    Serial.println("config:" + configToJson(config));
    dimUp();
    startWifi();
    startWebSocketServer();
    startWebServer();
    startMDNS(config.domainName);
    blink(2, 1023);
}

void loop() {
    webSocket.loop();
    server.handleClient();
}

void startWifi() {
    Serial.printf("Connecting to AP: %s", config.ssid);
    wifiMulti.addAP(config.ssid, config.password);
    int connectionTime = 0;
    while(wifiMulti.run() != WL_CONNECTED && connectionTime < WIFI_TIMEOUT) {
        delay(1000);
        connectionTime++;
        Serial.print(".");
    }

    if (wifiMulti.run() != WL_CONNECTED) {
        Serial.printf("Connection to AP failed. Creating AP: %s\r\n", WIFI_SOFT_AP_SSID);
        blink(6, 1023);
        
        IPAddress local_IP(192,168,1,1);
        IPAddress subnet(255,255,255,0);
        Serial.print("Setting soft-AP configuration ... ");
        Serial.println(WiFi.softAPConfig(local_IP, local_IP, subnet) ? "Ready" : "Failed!");
        WiFi.softAP(WIFI_SOFT_AP_SSID, "");
        Serial.print("Soft-AP IP address: ");
        Serial.println(WiFi.softAPIP());
    } else {
        Serial.print("connected. IP address:");
        Serial.println(WiFi.localIP());
    }
}

void startWebSocketServer() {
    webSocket.begin();
    webSocket.onEvent(webSocketEvent);
}

void startMDNS(char* dnsName) {
    if(MDNS.begin(dnsName)) {
        Serial.println("MDNS responder started: " + String(dnsName));
    }

    MDNS.addService("http", "tcp", 80);
    MDNS.addService("ws", "tcp", 81);
}

void startWebServer() {
    server.on("/", []() {
        server.send(200, "text/html", "<html> <head> <script> var connection = new WebSocket('ws://' + location.hostname + ':81/', ['arduino']); connection.onopen = function () { showStatus('connected'); connection.send('Connect ' + new Date()); }; connection.onclose = function (e) { showStatus('closed'); console.log('Server: ', e); }; connection.onerror = function (error) { document.getElementById('info').textContent = error; console.log('WebSocket Error ', error); }; connection.onmessage = function (e) { var type = e.data.substring(0, 1); switch (type) { case 'i': showInfo(e.data.substring(1)); break; case 'l': document.getElementById('ledValue').value = e.data.substring(1); break; case 'c': var config = JSON.parse(e.data.substring(1)); document.getElementById('ledValue').value = config.ledValue; document.getElementById('ssid').value = config.ssid; document.getElementById('password').value = config.password; document.getElementById('domainName').value = config.domainName; document.getElementById('dimTime').value = config.dimTime; break; } console.log('Server: ', e.data); }; function sendLedValue(value) { var ledValue = parseInt(value).toString(10); /*ledValue = ledValue.padStart(2, '0');*/ console.log('ledValue: ' + ledValue); connection.send('l' + ledValue); } function saveConfig() { var config = { ledValue : document.getElementById('ledValue').value.toString(10), ssid : document.getElementById('ssid').value, password : document.getElementById('password').value, domainName : document.getElementById('domainName').value, dimTime : document.getElementById('dimTime').value }; var configStringified = JSON.stringify(config); console.log('config: ' + configStringified); connection.send('c' + configStringified); } function loadConfig() { console.log('loadConfig()'); connection.send('r'); } function resetEesp8266() { console.log('resetEesp8266()'); connection.send('d'); } function showStatus(message) { document.getElementById('status').textContent = message; } function showInfo(message) { document.getElementById('info').textContent = message; } function showHideConfig() { var divConfig = document.getElementById('divConfig'); var displayValue = (divConfig.style.display === 'none' ? 'block' : 'none'); divConfig.style.display = displayValue; } </script> <style> .row { display: flex; justify-content: space-around; } button { width: 100%; margin: 30px 2px; padding: 50px 64px; border: none; background-color: #4CAF50; color : white; font-size: 60px; } button.showHideConfig { padding: 0px 64px; margin: 30px 0px 2px; border: 3px solid #4CAF50; background-color: #FFFFFF; color: #4CAF50; } input { height: 60px; width: 100%; padding: 12px 12px; font-size: 40px; /* margin: 0px 2px; */ margin: 12px 0px; } input[type=range] { -webkit-appearance: none; /* Hides the slider so that custom slider can be made */ width: 99%; /* Specific width is required for Firefox. */ background: transparent; /* Otherwise white in Chrome */ } input[type=range]::-webkit-slider-runnable-track { width: 99%; height: 30px; cursor: pointer; background: #4CAF50; border-radius: 1.3px; border: 0.2px solid #010101; margin: 20px 0px; } input[type=range]::-webkit-slider-thumb { -webkit-appearance: none; height: 100px; width: 100px; border-radius: 3px; background: #ffffff; cursor: pointer; margin-top: -35px; /* You need to specify a margin in Chrome, but in Firefox and IE it is automatic */ box-shadow: 1px 1px 1px #000000, 0px 0px 1px #0d0d0d; /* Add cool effects to your sliders! */ } #h1info { height: 40px; padding: 10px 10px; position: absolute; bottom: 10px; width: 96%; border: 3px solid #4CAF50; } </style> </head> <body onload='showHideConfig()'> <h1>ESP8266 LED CONTROL: <span id='status'>-</span></h1> <br/> <input id='ledValue' type='range' min='0' max='1023' value='0' step='16' oninput='sendLedValue(this.value);' /> <br/> <br/> <div class='row'> <button type='button' onclick='sendLedValue(this.value); document.getElementById(\"ledValue\").value = this.value; ' value='0'>OFF</button> <button type='button' onclick='sendLedValue(this.value); document.getElementById(\"ledValue\").value = this.value; ' value='1023'>ON</button> </div> <button type='button' class='showHideConfig' onclick='showHideConfig()' >&#8681; Config &#8681;</button> <div id='divConfig'> <div class='row'> <!-- <button type='button' onclick='connection.send(\"$\"); ' >Save</button> --> <button type='button' onclick='saveConfig(); ' >Save</button> <button type='button' onclick='loadConfig(); ' >Load</button> <button type='button' onclick='resetEesp8266(); ' >Reset</button> </div> <div class='row'> <input id='domainName' type='text' placeholder='domain name' title='domain name. Esp8266 can be reached using this name. Does not work on Android.'/> <input id='dimTime' type='number' min='0' max='9999' placeholder='dimm time in ms' title='dimm time in ms'/> </div> <div class='row'> <input id='ssid' name='wifi' type='text' placeholder='ssid' title='ssid'/> <input id='password' name='wifi' type='text' placeholder='password' title='password'/> </div> <!-- <h1 id='h1info'>Server: <span id='info'>-</span></h1> --> </div> </body> </html>");
    });
    
    server.begin();
}

void led(short value) {
//     analogWrite(PIN_LED, value);
//    analogWrite(PIN_LED, map(pow(value, 1.5), 0, 32720, 0, 767));
    analogWrite(PIN_LED, map(pow(value, 1.3), 0, 8181, 0, 400));
    
}
